package com.polarcape.spring.sockets.models;

import java.util.Date;

/**
 * Created by bojan on 5.10.15.
 */
public class MessageWrapper extends Message {
    private Date dateSent;

    public MessageWrapper() {}

    public MessageWrapper(String content, String nickname, Date dateSent) {
        super(content, nickname);
        this.dateSent = dateSent;
    }

    public Date getDateSent() {
        return dateSent;
    }

    public void setDateSent(Date dateSent) {
        this.dateSent = dateSent;
    }
}
