package com.polarcape.spring.sockets.models;

import java.util.Date;

/**
 * Created by Trajkovski on 26-Sep-15.
 */
public class Message {

    private String content;
    private String nickname;

    public Message() {}

    public Message(String content, String nickname) {
        this.content = content;
        this.nickname = nickname;
    }

    public String getContent() {
        return content;
    }

    public String getNickname() {
        return nickname;
    }
}