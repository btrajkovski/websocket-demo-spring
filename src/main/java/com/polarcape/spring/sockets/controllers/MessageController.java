package com.polarcape.spring.sockets.controllers;

import com.polarcape.spring.sockets.models.Message;
import com.polarcape.spring.sockets.models.MessageWrapper;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import java.util.Date;

/**
 * Created by Trajkovski on 26-Sep-15.
 */
@Controller
public class MessageController {

    @MessageMapping("/chat")
    @SendTo("/topic/messages")
    public MessageWrapper echo(Message message) throws Exception {
        return new MessageWrapper(message.getContent(), message.getNickname(), new Date());
//        return string;
    }
}